from main import nod


def test_1():
# НОД при N > M
    assert nod(140,96) == 4


def test_2():
# НОД при M > N
    assert nod(96,140) == 4


def test_3():
# НОД при одинаковых значениях N и M
    assert nod(96,96) == 96


def test_4():
# НОД при остатке равно нулю
    assert nod(192,96) == 96