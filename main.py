def nod(n,m):
    a,b = max(n,m), min(n,m)
    c = a % b
    print(f'Делим {a} на {b}, остаток от деления = {c}')
    if c!=0:
        return nod(b,c)
    return b


if __name__ =='__main__':
    print(nod(140,96))
